import mongoose from 'mongoose'

mongoose.set('useCreateIndex', true);
// mongoose.connect('mongodb://localhost/disk', { useNewUrlParser: true })
mongoose.connect('mongodb://localhost/aplicativodisq28122018', { useNewUrlParser: true })
mongoose.Promise = global.Promise

export default mongoose