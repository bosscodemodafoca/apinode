import Os from '../models/os'
import moment from 'moment'

async function getGarantiaRestante(loja){
    // ###########################################
    var turno = loja.turnos
    let hoje = moment()
  
    let numero_turno = 0
    var hora
    for (let i = 0; i < turno.length; i++) {
      hora = turno[i].inicio.split(':')
      var turno_atual = moment(hoje).startOf('day')
      turno_atual.set({ hour: hora[0], minute: hora[1] })
      if (hoje.isAfter(turno_atual)) {
        numero_turno++
      }
    }
    // console.log(numero_turno)
    var inicio = moment(hoje)
  
    if (numero_turno == 0) {
      numero_turno = turno.length
      inicio = inicio.subtract(1, 'days')
    }
  
    inicio = inicio.startOf('day')
    const data_contabilidade = moment(inicio)
    hora = turno[numero_turno - 1].inicio.split(':')
    inicio = inicio.set({ hour: hora[0], minute: hora[1] })
  
    var fim = moment(inicio)
    if (numero_turno == turno.length) {
      fim = fim.add(1, 'days')
      hora = turno[0].inicio.split(':')
      fim = fim.set({ hour: hora[0], minute: hora[1] })
    } else {
      hora = turno[numero_turno].inicio.split(':')
    }
  
    fim = fim.set({ hour: hora[0], minute: hora[1] })
  
    var garantia_restante = loja.turnos[numero_turno - 1].garantia
    var today = moment().startOf('day')
    var tomorrow = moment(today).endOf('day')
    const pedidos_do_dia = await Os.find({
      loja: loja._id,
      status: { $ne: 'CANCELADA' },
      created_at: {
        $gte: inicio.toDate(),
        $lt: fim.toDate()
      }
    }).limit(loja.turnos[numero_turno - 1].garantia)
  
    pedidos_do_dia.map(item => {
      garantia_restante--
    })
  // console.log({garantia_restante,numero_turno})
    return {garantia_restante,numero_turno,data_contabilidade}
  
  // ###########################################
  }

  export default getGarantiaRestante