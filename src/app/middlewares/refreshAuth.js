import jwt from 'jsonwebtoken'
import authConfig from '../../config/jwtConfig'

module.exports = (req, res, next) => {
  const token = req.body.token
  if (!token) return res.status(401).send({ error: 'no token provided' })
  jwt.verify(token, authConfig.secretRefreshToken, (err, decoded) => {
    if (err) return res.status(401).send({ error: 'token invalid' })
    req.params = decoded.id
    return next()
  })
}
