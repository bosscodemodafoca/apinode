import jwt from 'jsonwebtoken'
import authConfig from '../../config/jwtConfig'
import Loja from '../models/loja'
import Admin from '../models/admin'
import Motoboy from '../models/motoboy'

module.exports =  (req, res, next) => {
  const authHeader = req.headers.authorization

  if (!authHeader) return res.status(401).send({ error: 'no token provided' })

  const parts = authHeader.split(' ')
  if (!parts.length === 2) return res.status(401).send({ error: 'token error' })

  const [schema, token] = parts
  if (!/^Bearer$/i.test(schema)) {
    return res.status(401).send({ error: 'Token malformated' })
  }

  jwt.verify(token, authConfig.secretToken, async (err, decoded) => {

    if (err) return res.status(401).send({ error: 'token invalid' })
    // console.log(decoded)
    let modelo = null
    switch (decoded.tipo) {
      case 'admin':
        modelo = Admin
        break
      case 'loja':
        modelo = Loja
        break
      case 'motoboy':
        modelo = Motoboy
        break
      default:
        return res.status(401).send()
    }
    const user = await modelo.findOne({_id:decoded.id,ativo:true})
    // console.log(user)
    if (!user) return res.status(401).send({ error: 'Usuário Inativo' })
    req.userId = decoded.id
    req.userTipo = decoded.tipo
    req.token = token
    req.user = user
    return next()
  })
}
