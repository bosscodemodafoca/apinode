import mongoose from '../../database'
require('mongoose-double')(mongoose);

import {BairroSchema,AreaSchema} from './loja'

const OsSchema = new mongoose.Schema({
  bairro: BairroSchema,
  area: AreaSchema,
  valor: {
    type: mongoose.Schema.Types.Double,
    required:true
  },
  loja: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Loja',
    required:true
  },
  motoboy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Motoboy'
  },
  status: {
    type: String,
    default: "PENDENTE"
  },
  pago_loja:{
    type: String,
    default: "A PAGAR"
  },
  pago_motoboy:{
    type: String,
    default: "A PAGAR"
  },
  os: {
    type: Number,
    required:true
  },
  turno: {
    type: Number,
    required:true
  },
  data_contabilidade:{
    type:Date
  },
  aceito: {
    type: Date,
  },
  concluido: {
    type: Date,
  },
  created_at: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model('Os', OsSchema)
