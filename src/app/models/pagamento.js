import mongoose from '../../database'
require('mongoose-double')(mongoose);

const PagamentoSchema = new mongoose.Schema({
  valor: {
    type: mongoose.Schema.Types.Double,
    required:true
  },
  loja: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Loja'
  },
  motoboy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Motoboy'
  },
  os: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Os'
  }],
  turno: {
    type: Number,
    required:true
  },
  data_contabilidade:{
    type:Date
  },
  created_at: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model('Pagamento', PagamentoSchema)
