import mongoose from "../../database";

const CodigoSchema = new mongoose.Schema({
  codigo: {
    type: String,
    required: true,
  },
  tipo:{
    type: String,
    required: true,
  },
  usuario: {
    type: String,
    required: true,
  },
  validade: {
    type: Date,
    required: true,
  }
})

export default mongoose.model("Codigo", CodigoSchema)

