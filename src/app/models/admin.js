import mongoose from "../../database";

const AdminSchema = new mongoose.Schema({
  ativo:{
    type:Boolean,
    default:true
  },
  nome: {
    type: String,
    required: true,
  },
  email:{
    type: String,
    required: true,
  },
  celular:{
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  created_at: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model("Admin", AdminSchema)

