import mongoose from '../../database'

const MotoboySchema = new mongoose.Schema({
  ativo: {
    type: Boolean,
    default: false
  },
  deletado: {
    type: Boolean
  },
  nome: {
    type: String,
    required: [true, 'O campo NOME é obrigatório'],
  },
  email: {
    type: String,
    required: [true, 'O campo E-MAIL é obrigatório'],
  },
  whatsapp: {
    type: String,
    required: [true, 'O campo WHATSAPP é obrigatório'],
  },
  cpf: {
    type: String,
    required: [true, 'O campo CPF é obrigatório'],
  },
  placa: {
    type: String,
    required: [true, 'O campo PLACA DA MOTO é obrigatório'],
  },
  celular: {
    type: String,
    required: [true, 'O campo CELULAR é obrigatório'],
  },
  observacoes: {
    type: String
  },
  password: {
    type: String,
    required: [true, 'O campo SENHA é obrigatório'],
    select: false
  },
  notification:{
    type:String
  },
  created_at: {
    type: Date,
    default: Date.now
  }
})


export default mongoose.model('Motoboy', MotoboySchema)
