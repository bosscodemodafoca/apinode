import mongoose from '../../database'
require('mongoose-double')(mongoose)

export const BairroSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: [true, 'O campo BAIRRO é obrigatório']
     
  },
  rua: String
})
export const AreaSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: [true, 'O campo AREA 1 é obrigatório'],
  },
  km: {
    type: String,
    required: [true, 'O campo KM DA AREA 1 é obrigatório'],
  },
  valor: {
    type: mongoose.Schema.Types.Double,
    required: [true, 'O campo VALOR DA AREA 1 é obrigatório'],
  },
  bairro:{type:[BairroSchema],default:[]}
})

const TurnoSchema = new mongoose.Schema({
  inicio:{
    type: String,
    required: true,
    validate: {
      validator: function(v) {
        return /\d{2}\:\d{2}/.test(v);
      },
      message: props => `${props.value} is not a valid phone number!`
    }
  },
  garantia:{
    type: Number,
    required: true,
    default:0
  },
  diaria_motoboy:{
    type: mongoose.Schema.Types.Double,
    required: true,
    default:0
  },
  diaria_loja:{
    type: mongoose.Schema.Types.Double,
    required: true,
    default:0
  }
})

const LojaSchema = new mongoose.Schema({
  ativo: {
    type: Boolean,
    default: false
  },
  deletado: {
    type: Boolean
  },
  nome: {
    type: String,
    required: [true, 'O NOME bairro é obrigatório'],
  },
  telefone: {
    type: String,
    required: [true, 'O campo TELEFONE é obrigatório'],
  },
  endereco: {
    type: String,
    required: [true, 'O campo ENDERECO é obrigatório'],
  },
  proprietario: {
    type: String,
    required: [true, 'O campo PROPRIETARIO é obrigatório'],
  },
  celular_proprietario: {
    type: String,
    required: [true, 'O campo WATSAPP DO PROPRIETARIO é obrigatório'],
  },
  responsavel: {
    type: String,
    required: [true, 'O campo RESPONSÁVEL é obrigatório'],
  },
  email: {
    type: String,
    required: [true, 'O campo E-MAIL é obrigatório'],
  },
  motoboys: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Motoboy'
      }
    ],
    default: []
  },
  celular: {
    type: String,
    required: [true, 'O campo WATSAPP do RESPONSÁVEL é obrigatório'],
  },
  area1: {
    type: AreaSchema,
    required: true
  },
  areas:[AreaSchema],
  turnos:[TurnoSchema],
  observacoes: {
    type: String
  },
  password: {
    type: String,
    required: [true, 'O campo SENHA é obrigatório'],
    select: false
  },
  created_at: {
    type: Date,
    default: Date.now
  }
})
export default mongoose.model('Loja', LojaSchema)