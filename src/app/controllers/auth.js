import express from 'express'
import Admin from '../models/admin'
import Loja from '../models/loja'
import Motoboy from '../models/motoboy'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import jwtConfig from '../../config/jwtConfig.js'

const router = express.Router()

// a funcao login cria o token jtw
// token normal e token de atualizacao
function login (params = {}) {
  const token = jwt.sign(params, jwtConfig.secretToken, {
    expiresIn: jwtConfig.tokenLife
  })
  const refreshToken = jwt.sign(params, jwtConfig.secretRefreshToken, {
    expiresIn: jwtConfig.refreshTokenLife
  })
  return { token, refreshToken }
}

router.post('/novoadm', async (req, res) => {
  const tem = await Admin.findOne({celular:'admin'})
  if (!tem){
  const hash = await bcrypt.hash('123123', 10)
  const user = await Admin.create({
    nome: 'admin',
    email: 'aplicativo.disqmotoboy@gmail.com', //senha jesus1020
    celular: 'admin',
    password: hash
  })
  return res.send({ msg: 'Usuário Criado!' })
}else{
  return res.send({ msg: 'Usuário já existe!' })
}
})

router.post('/register/:tipo', async (req, res) => {
  // sempre que tiver uma senha no corpo da requisicao
  // criptografar antes de salvar
  if (req.body.password) {
    if (req.body.password == '') {
      req.body.password = undefined
    } else {
      const hash = await bcrypt.hash(req.body.password, 10)
      req.body.password = hash
    }
  }
  // dependendo do tipo da requisicao admin/motoboy/loja
  // é atualizado com o corpo da requisicao caso tem _id
  // caso não é criado um novo registro
  try {
    let user = null
    switch (req.params.tipo) {
      case 'admin':
        if (req.body._id) {
          user = await Admin.findByIdAndUpdate(req.body._id, req.body, {
            new: true
          })
        }
        if (!user) user = await Admin.create(req.body)
        break
      case 'loja':
        if (req.body._id) {
          user = await Loja.findByIdAndUpdate(req.body._id, req.body, {
            new: true
          })
          req.io.to(req.body._id).emit('DELETAR_MOTOBOY')
        }
        if (!user) user = await Loja.create(req.body)
        break
      case 'motoboy':
        if (req.body._id) {
          user = await Motoboy.findByIdAndUpdate(req.body._id, req.body, {
            new: true
          })
          req.io.emit('DELETAR_MOTOBOY')
        }
        if (!user) {
          user = await Motoboy.create(req.body)
        }
        break
      default:
        return res.status(404).send()
    }
    user.password = undefined
    return res.send(user)
  } catch (erro) {
    console.log(erro)
    // Casso aconteça algum erro de validacao retorna as mensagens do validador
    // do mongoose
    if (erro.errors) {
      return res.status(400).send({ error: 'VALIDATION', errors: erro.errors })
    }
    return res
      .status(400)
      .send({ error: 'Não foi possível realizar o cadastro!' })
  }
})

// AUTENTICA
router.post('/authenticate/:tipo', async (req, res) => {
  console.log(req.body)

  const tipo = req.params.tipo
  let User = null
  switch (tipo) {
    case 'admin':
      User = Admin
      break
    case 'loja':
      User = Loja
      break
    case 'motoboy':
      User = Motoboy
      break
    default:
      return res.status(404).send()
  }

  // procura pelo email ou celula

  const user = await User.findOne({
    $or: [{ celular: req.body.user }, { email: req.body.user }]
  }).select('+password')
  if (!user) return res.status(400).send({ error: 'Usuário não encontrado!' })

  if (!(await bcrypt.compare(req.body.password, user.password))) {
    return res.status(400).send({ error: 'Senha inválida' })
  }

  if (!user.ativo) {
    return res.status(400).send({ error: 'Usuário Inativo!' })
  }
  user.password = undefined
  if (tipo == 'loja') {
    // caso seja do tipo loja
    // verifica se o cadastro esta completo para aceitar o login
    // console.log("LOJA LOGANDO")
    var qtdbairros = 0
    user.area1.bairro.map(() => qtdbairros++)
    user.areas.map(area => area.bairro.map(() => qtdbairros++))

    if (
      qtdbairros <= 0 ||
      user.turnos.length == 0 ||
      user.motoboys.length == 0
    ) {
      return res
        .status(400)
        .send({ error: 'Cadastro incompleto, contate o Admin!' })
    }
    // console.log("PASSOU 8O")
  }
  // responde com os tokens
  const token = login({ id: user.id, tipo: req.params.tipo })
  const response = { ...token, user }
  return res.send({ response })
})

module.exports = app => app.use('/auth', router)
