import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import auth from '../middlewares/auth'
import Motoboy from '../models/motoboy'
import Loja from '../models/loja'
const router = express.Router()
router.use(auth)
router.get('/me', async (req, res) => {
 
  // retorna os dados do motoboy
  return res.send(req.user)
})
router.get('/', async (req, res) => {
  // retorna todos os motoboys nao deletados para o admin
  // e retorna todos os motoboys da loja para a aloja
  let result = null
  switch (req.userTipo) {
    case 'admin':
      result = await Motoboy.find({ deletado: { $exists: false } })
      return res.send(result)
    case 'loja':
      result = await Loja.findById(req.userId).populate('motoboys')
      const motoboys = result.motoboys.filter(moto => {
        if (moto.ativo) return moto
      })

      return res.send(motoboys)
    default:
      return res.status(404).send()
  }
})

router.post('/deletar', async (req, res) => {
  // deleta um motoboy
  const id = req.body._id
  if (req.userTipo != 'admin') return res.status(401).send()
  
  const motoboy_atualizado = await Motoboy.findByIdAndUpdate(
    id,
    { ativo: false, deletado: true },
    { new: true }
  )
  req.io.emit('DELETAR_MOTOBOY')
  return res.status(200).send()
})

module.exports = app => app.use('/motoboy', router)
