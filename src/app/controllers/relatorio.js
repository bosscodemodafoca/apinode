import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import auth from '../middlewares/auth'
import Motoboy from '../models/motoboy'
import Loja from '../models/loja'
import Pagamento from '../models/pagamento'
import Recebimento from '../models/recebimento'
import Os from '../models/os'
import moment from 'moment'
const router = express.Router()

router.get('/', async (req, res) => {
  const result = await Os.find()
  return res.send(result)
})
router.use(auth)

// ###############################################################################
// ###############################################################################
router.post('/apagar', async (req, res) => {
  if (req.userTipo != 'loja') return res.status(401).send()
  const loja_id = req.userId
  const loja = await Loja.findById(loja_id)

  const os = await Os.find({
    loja: loja_id,
    pago_loja: { $ne: 'PAGO' }
  })
    .and([{ status: { $ne: 'PENDENTE' } }, { status: { $ne: 'CANCELADA' } }])
    .populate('loja')
    .populate('motoboy')
    .sort({ data_contabilidade: -1 })
    .sort({ turno: 1 })
    .sort({ motoboy: 1 })

  var turnos = []
  var valor_total = 0

  var temp_valor = 0
  var temp_os = []
  var data_atual = ''
  var turno_atual = ''
  var motoboys = []
  var primeiro = true
  var motoboy_atual = ''
  // var motoboy_pop = null
  const prom = os.map(async (item, index) => {
    if (
      data_atual.toString() != item.data_contabilidade.toString() ||
      turno_atual != item.turno ||
      motoboy_atual._id != item.motoboy._id
    ) {
      if (primeiro) {
        primeiro = false
      } else {
        // motoboy_pop = await Motoboy.findById(motoboy_atual)
        const novo = {
          valor: temp_valor,
          os: temp_os,
          data: data_atual,
          turno: turno_atual,
          motoboy: motoboy_atual
        }
        turnos.push(novo)
        valor_total += parseFloat(temp_valor)
      }
      // console.log('loja.diaria', item.loja)
      data_atual = item.data_contabilidade
      turno_atual = item.turno
      temp_valor = parseFloat(loja.turnos[item.turno - 1].diaria_loja.value) + 0
      motoboys = []
      temp_os = []
      motoboy_atual = item.motoboy
    }
   
      temp_os.push(JSON.parse(JSON.stringify(item)))
      temp_valor += parseFloat(item.valor.value)
    
    // console.log(item)
    if (!motoboys.includes(item.motoboy)) {
      motoboys.push(item.motoboy)
    }
  })

  Promise.all(prom).then(async () => {
    // motoboy_pop = await Motoboy.findById(motoboy_atual)
    // console.log(motoboy_pop)
    const novo = {
      valor: temp_valor,
      os: temp_os,
      data: data_atual,
      turno: turno_atual,
      motoboy: motoboy_atual
    }
if (!primeiro){
    turnos.push(novo)
    valor_total += parseFloat(temp_valor)
    }
    const resultado = { valor_total, turnos: turnos }
    // console.log("solicitacao",motoboy_pop)
    return res.status(200).send(resultado)
  })
})
// ###############################################################################
router.post('/areceber', async (req, res) => {
  if (req.userTipo != 'motoboy') return res.status(401).send()
  const motoboy_id = req.userId
  const motoboy = await Motoboy.findById(motoboy_id)

  const os = await Os.find({
    motoboy: { $eq: motoboy_id, $exists: true },
    pago_motoboy: { $ne: 'PAGO' },
    status: { $ne: 'CANCELADA' }
  })
    .populate('loja')
    .populate('motoboy')
    .sort({ loja: 1 })
    .sort({ data_contabilidade: 1 })
    .sort({ turno: 1 })
    .sort({ motoboy: 1 })

  var turnos = []
  var valor_total = 0
  var temp_valor = 0
  var temp_os = []
  var data_atual = ''
  var turno_atual = ''
  var loja_atual = ''
  var primeiro = true
  os.map((item, index) => {
   
    // console.log(
    //   '1',
    //   data_atual.toString() != item.data_contabilidade.toString()
    // )
    // console.log(
    //   '1',
    //   data_atual.toString() + '|' + item.data_contabilidade.toString()
    // )
    // console.log("2",turno_atual != item.turno)
    // console.log("2",turno_atual +'|'+ item.turno)
    if (
      data_atual.toString() != item.data_contabilidade.toString() ||
      turno_atual != item.turno ||
      loja_atual._id != item.loja._id
    ) {
      if (primeiro) {
        primeiro = false
      } else {
        const novo = {
          valor: temp_valor,
          os: temp_os,
          data: data_atual,
          turno: turno_atual,
          loja: loja_atual
        }
        console.log(data_atual)
        turnos.push(novo)
        valor_total += parseFloat(temp_valor)
      }
      // console.log("loja.diaria",loja)
      data_atual = item.data_contabilidade
      turno_atual = item.turno
      loja_atual = item.loja
      // if (!item.loja)
      // console.log(item)
      // console.log(item.loja)
      temp_valor =
        parseFloat(item.loja.turnos[item.turno - 1].diaria_motoboy.value) + 0
      temp_os = []
    }
    temp_os.push(JSON.parse(JSON.stringify(item)))
    temp_valor += parseFloat(item.valor.value)
  })
  const novo = {
    valor: temp_valor,
    os: temp_os,
    data: data_atual,
    turno: turno_atual,
    loja: loja_atual
  }
  if (!primeiro) {
    console.log(data_atual)
    turnos.push(novo)
    valor_total += parseFloat(temp_valor)
  }
  const resultado = { valor_total, turnos: turnos }
  // console.log(resultado)
  return res.status(200).send(resultado)
})
// ###############################################################################
// ###############################################################################
router.post('/recebidos', async (req, res) => {
  if (req.userTipo != 'motoboy') return res.status(401).send()

  var de = moment(req.body.de).startOf('day')
  var ate = moment(req.body.ate).endOf('day')
  const motoboy_id = req.userId
  const motoboy = await Motoboy.findById(motoboy_id)

  const pagamentos = await Pagamento.find({
    motoboy: motoboy_id,
    created_at: { $gte: de, $lte: ate }
  })
    .sort({ created_at: -1 })
    .populate('loja')
    .populate('motoboy')
    .populate({ path: 'os', populate: { path: 'motoboy' } })

  console.log(' rec', pagamentos)
  return res.status(200).send(pagamentos)
})
// ###############################################################################
// ###############################################################################
// ###############################################################################
router.post('/pagos', async (req, res) => {
  if (req.userTipo != 'loja') return res.status(401).send()

  var de = moment(req.body.de).startOf('day')
  var ate = moment(req.body.ate).endOf('day')
  const pagamentos = await Recebimento.find({
    loja: req.userId,
    created_at: { $gte: de, $lte: ate }
  })
    .sort({ created_at: -1 })
    .populate('loja')
    .populate('motoboy')
    .populate({ path: 'os', populate: { path: 'motoboy' } })

  return res.status(200).send(pagamentos)
})
// ###############################################################################
module.exports = app => app.use('/relatorio', router)
