import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import auth from '../middlewares/auth'
import Motoboy from '../models/motoboy'
import Loja from '../models/loja'
import Os from '../models/os'
import moment from 'moment'
import axios from 'axios'
import admin from '../models/admin'
import getGarantiaRestante from '../functions/getGarantiaRestante'
const router = express.Router()
router.use(auth)

router.post('/pendentes', async (req, res) => {
  // retorna as os pendentes
  const loja = req.body.loja
  if (req.userTipo != 'admin') return res.status(401).send()

  const result = await Os.find({
    loja: loja,
    status: 'PENDENTE'
  })
    .populate('motoboy')
    .populate('loja')
  return res.send(result)
})

router.get('/', async (req, res) => {
  const user_id = req.userId
  if (req.userTipo == 'loja') {
    var loja =  req.user
    var {numero_turno,data_contabilidade} = await getGarantiaRestante(loja)
    var inicio = moment(data_contabilidade)
   
    var turno = loja.turnos
    var hora = turno[numero_turno - 1].inicio.split(':')
    inicio = inicio.set({ hour: hora[0], minute: hora[1] })
    
    
    const result = await Os.find({
      $and:[
        {loja: user_id},
        {$or:[{created_at: {$gte: inicio.toDate()}}, {status: 'ACEITO'},{status: 'PENDENTE'}]}

      ]
      
    })
      .populate('motoboy')
      .populate('loja')
    // retorna as os de uma loja criadas nos ultimos 2 dias
    return res.send(result)
  } else {
    //  faz listagem das lojas q o motoboy faz parte
    const lojas = await Loja.find({ ativo: true }).populate('motoboys')
    const lojas_id = []
    lojas.map((loja, index) => {
      loja.motoboys.map((motoboy, m_index) => {
        // console.log(motoboy._id, user_id)
        if (motoboy._id == user_id) {
          if (!lojas_id.includes(loja._id)) lojas_id.push(loja._id)
        }
      })
    })
    // caso seja um motoboy
    // retorna todas as os das lojas em q ele faz parte
    // onde o status seja aceito ou pendente
    const result = await Os.find({
      $and: [
        { $or: [{ motoboy: { $exists: false } }, { motoboy: user_id }] },
        { $or: [{ status: 'ACEITO' }, { status: 'PENDENTE' }] },
        { loja: { $in: lojas_id } }
      ]
    })
      .populate('motoboy')
      .populate('loja')
    return res.send(result)
  }
})

// Rota para Criar uma Nova OS
// Acessada pela LOJA
// Emmit a OS para a LOJA e para os motoboys Associados a loja
router.post('/nova', async (req, res) => {
  var notifications = []
  const loja_id = req.userId
  const motoboy_id = req.body.motoboy._id
  // Pesquisa a loja
  const loja = await Loja.findById(loja_id).populate('motoboys')
  // Se nenhuma lojá for encontrada nega o acesso
  if (!loja) return res.status(401).send()
  // INICIALIZA O NUMERO DA OS E NUMERO DO PEDIDO

  // console.log(garantia_restante)

  var {
    garantia_restante,
    numero_turno,
    data_contabilidade
  } = await getGarantiaRestante(loja)

  var n_os = 0
  const ultimo_os = await Os.find()
    .sort({ os: -1 })
    .limit(1)
  // pega o numero da ultima OS
  ultimo_os.map(item => {
    if (item.os) n_os = item.os
  })

  // PERCORRE TODOS OS SETORES ENVIADOS
  // PARA SAVER
  const todos = Object.keys(req.body.setores).map(async key => {
    const item = req.body.setores[key]
    // console.log(item.setor)
    for (let i = 0; i < item.quantidade; i++) {
      // INCREMENTA O NUMERO DE PEDIDO E OS ANTES DE SALVAR CADA UM
      n_os++
      // Calcula o saldo da garantia e o valor do pedido atual
      var valor_pedido = parseFloat(item.setor.area.valor)
      // se estiver na garantia faz o desconto
      if (garantia_restante > 0) {
        valor_pedido =
          parseFloat(item.setor.area.valor) - parseFloat(loja.area1.valor.value)
        garantia_restante--
      }

      // CRIA A OS NO BANCO DE DADOS
      const os_create = await Os.create({
        loja: loja_id,
        motoboy: motoboy_id,
        bairro: {
          _id: item.setor._id,
          nome: item.setor.nome,
          rua: item.setor.rua
        },
        area: item.setor.area,
        os: n_os,
        valor: valor_pedido,
        data_contabilidade: data_contabilidade,
        turno: numero_turno
      })

      const os = await Os.findById(os_create._id)
        .populate('motoboy')
        .populate('loja')
      // AO FIM EMITE PARA OS MOTOBOYS REFERENTES E LOJA
      // atravez de socket

      req.io.to(loja_id).emit('ADD_OS', os)
      // console.log(garantia_restante + '/' + loja.turnos[0].garantia)
      req.io
        .to(loja_id)
        .emit(
          'SET_SALDO',
          garantia_restante +
            '/' +
            loja.turnos[numero_turno - 1].garantia +
            ' #' +
            numero_turno
        )
      // se o motoboy tiver sido selecionado
      // adiciona na lista a notificacao
      if (os.motoboy) {
        const motoboy = await Motoboy.findById(os.motoboy._id)
        req.io.to(os.motoboy._id).emit('ADD_OS', os)
        if (motoboy.notification) {
          if (!notifications.includes(motoboy.notification)) {
            notifications.push(motoboy.notification)
          }
        }
      } else {
        // caso nao percorre todos os motoboys da loja
        // e adiciona na lista a notificacao
        loja.motoboys.map((item, index) => {
          if (item.notification) {
            if (!notifications.includes(item.notification)) {
              notifications.push(item.notification)
            }
          }
          req.io.to(item._id).emit('ADD_OS', os)
        })
      }
    }
  })
  // RETORNA O STATUS 201 CREATED

  Promise.all(todos).then(() => {
    notifications.map(async (_id, index) => {
      const notification = {
        to: _id,
        foreground: false,
        userInteraction: false,
        data: {
          message: 'Novo Pedido Pendente'
        }
      }

      try {
        // envia a notificacao
        const r = await axios.post(
          'https://fcm.googleapis.com/fcm/send',
          notification,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'key=AIzaSyAzF75tRoBKUeftHMxDFkDobCjno_ONV50'
            }
          }
        )
        // console.log('TESTE')
      } catch (error) {}
    })
    // console.log('Deu certo', notifications)
  })

  return res.status(201).send()
})
// #####################################################################
router.post('/aceitar', async (req, res) => {
  // Se não for uma loja nega o acesso
  console.log('aceitar', req.body)
  if (req.userTipo != 'motoboy') return res.status(401).send()
  // SALVA O ID DA LOJA E DO MOTOBOY QUE VEM DO TOKEN DO MIDDLEWARE
  const motoboy_id = req.userId
  const lista = req.body.lista
  lista.map(async (item, index) => {
    const os = await Os.findById(item)

    if (os.status != 'PENDENTE') return res.status(400).send()

    if (os.motoboy != motoboy_id && !!os.motoboy) return res.status(400).send()

    const os_atualizada = await Os.findByIdAndUpdate(
      item,
      { status: 'ACEITO', aceito: moment(), motoboy: motoboy_id },

      {
        new: true
      }
    )
    // atualiza o status
    // envia  pra loja e para os motoboys a os atualizada

    const os_nova = await Os.findById(os_atualizada._id).populate('motoboy')

    req.io.to(os.loja).emit('ADD_OS', os_nova)
    if (!os.motoboy) {
      const loja = await Loja.findById(os.loja)
      loja.motoboys.map((item, index) => {
        req.io.to(item).emit('ADD_OS', os_nova)
      })
    } else {
      req.io.to(motoboy_id).emit('ADD_OS', os_nova)
    }
  })
  // RETORNA O STATUS 200 OK
  console.log('ok')
  return res.status(200).send()
})

router.post('/entregar', async (req, res) => {
  // console.log("req",req.body)
  // Se não for uma loja nega o acesso
  if (req.userTipo != 'motoboy') return res.status(401).send()
  // SALVA O ID DA LOJA E DO MOTOBOY QUE VEM DO TOKEN DO MIDDLEWARE
  const motoboy_id = req.userId
  const lista = req.body.lista
  lista.map(async (item, index) => {
    const os = await Os.findById(item)
    if (os.status != 'ACEITO') return res.status(400).send()

    if (os.motoboy != motoboy_id && !!os.motoboy) return res.status(400).send()
    const os_atualizada = await Os.findByIdAndUpdate(
      item,
      { status: 'CONCLUIDO', concluido: moment(), motoboy: motoboy_id },
      {
        new: true
      }
    )
    // atualiza o status
    // envia  pra loja e para os motoboys a os atualizada
    const os_nova = await Os.findById(os_atualizada._id).populate('motoboy')
    req.io.to(os.loja).emit('ADD_OS', os_nova)
    if (!os.motoboy) {
      const loja = await Loja.findById(os.loja)
      loja.motoboys.map((item, index) => {
        console.log('emitindo', item)
        req.io.to(item).emit('ADD_OS', os_nova)
      })
    } else {
      console.log('emitindo', motoboy_id)
      req.io.to(motoboy_id).emit('ADD_OS', os_nova)
    }
  })
  // RETORNA O STATUS 200 OK
  return res.status(200).send()
})

router.post('/cancelar', async (req, res) => {
  // Se não for uma loja nega o acesso
  if (req.userTipo != 'admin') return res.status(401).send()
  // SALVA O ID DA LOJA E DO MOTOBOY QUE VEM DO TOKEN DO MIDDLEWARE

  const os_atualizada = await Os.findByIdAndUpdate(
    req.body._id,
    { status: 'CANCELADA' },
    {
      new: true
    }
  )
  // atualiza o status
  // envia  pra loja e para os motoboys a os atualizada
  const os_nova = await Os.findById(os_atualizada._id).populate('motoboy')
  req.io.to(os_nova.loja).emit('ADD_OS', os_nova)
  if (!os_nova.motoboy) {
    const loja = await Loja.findById(os_nova.loja)
    loja.motoboys.map((item, index) => {
      console.log('emitindo', item)
      req.io.to(item).emit('ADD_OS', os_nova)
    })
  } else {
    console.log('emitindo', motoboy_id)
    req.io.to(motoboy_id).emit('ADD_OS', os_nova)
  }
  return res.status(200).send()
})

module.exports = app => app.use('/os', router)
