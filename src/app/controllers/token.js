import express from "express";
import jwt from "jsonwebtoken";
import jwtConfig from "../../config/jwtConfig.js";
import authConfig from '../../config/jwtConfig'
import Motoboy from '../models/motoboy'
const router = express.Router();
//ATUALIZA TOKEN

router.post("/refreshtoken", async (req, res) => {
    const token = req.body.token
    if (!token) return res.status(401).send({ error: 'no token provided' })
    jwt.verify(token, authConfig.secretRefreshToken, (err, decoded) => {
      if (err) return res.status(401).send({ error: 'token invalid' })
      console.log(decoded.id)
      
      const token = jwt.sign({ id: decoded.id,tipo: decoded.tipo},
         jwtConfig.secretRefreshToken, {expiresIn: jwtConfig.refreshTokenLife})
      return  res.send({token})
    })
   
})
router.post("/token", async (req, res) => {
  if (!req.body.token)
  return res.status(401).send({ error: 'no token provided' })
  const token = req.body.token
  jwt.verify(token, authConfig.secretRefreshToken, (err, decoded) => {
    if (err) return res.status(401).send({ error: 'token invalid' })
    const params = decoded.id
    const token = jwt.sign({ id: decoded.id,tipo: decoded.tipo}, jwtConfig.secretToken, {expiresIn: jwtConfig.tokenLife})
    
    return  res.send({token})
  })
})

router.post('/logout',async(req,res)=>{
  console.log("token/logout")
  const token = req.body.token
  if (!token) return res.status(401).send({ error: 'no token provided' })
  jwt.verify(token, authConfig.secretToken, async (err, decoded) => {
    if (err) {
      // console.log(err)
      return res.status(401).send({ error: 'token invalid' })
    }
    const params = decoded.id
   const motoboy = await Motoboy.findByIdAndUpdate(decoded.id, {notification:''}, {
      new: true
    })
    console.log(motoboy)
    return  res.send()
  })

})

router.post("/notification", async (req, res) => {

  console.log(req.body)
  const token = req.body.token
  if (!token) return res.status(401).send({ error: 'no token provided' })
  jwt.verify(token, authConfig.secretToken, async (err, decoded) => {
    if (err) {
      // console.log(err)
      return res.status(401).send({ error: 'token invalid' })
    }
    // console.log(decoded)
    const params = decoded.id
   const motoboy = await Motoboy.findByIdAndUpdate(decoded.id, {notification:req.body.notification}, {
      new: true
    })
    console.log(motoboy)
    return  res.send()
  })
})

module.exports = app => app.use("/token", router);
