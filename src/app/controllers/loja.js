import express from 'express'
import auth from '../middlewares/auth'
import Loja from '../models/loja'
import getGarantiaRestante from '../functions/getGarantiaRestante'
const router = express.Router()
router.use(auth)
router.get('/', async (req, res) => {
  // retorna todas as lojas
  const result = await Loja.find()
  return res.send(result)
})

router.get('/me', async (req, res) => {
  // retorna os dados da loja
  // para uma loja
  const loja = req.user
  if (req.userTipo != 'loja') return res.status(401).send()
  var {garantia_restante,numero_turno} = await getGarantiaRestante(loja)
  const loja_obj = JSON.parse(JSON.stringify(loja))
  const result = {
    ...loja_obj,
    saldo:
      garantia_restante +
      '/' +
      loja.turnos[numero_turno - 1].garantia +
      ' #' +
      numero_turno
  }
  return res.send(result)
})

router.post('/deletar', async (req, res) => {
  // deleta a loja
  const id = req.body._id
  if (req.userTipo != 'admin') return res.status(401).send()
 
  const loja = await Loja.findByIdAndUpdate(
    id,
    { ativo: false, deletado: true },
    { new: true }
  )
  return res.status(200).send()
})

// router.get('/bairro', async (req, res) => {
//   const result = await Loja.find()
//   return res.send(result)
// })

module.exports = app => app.use('/loja', router)
