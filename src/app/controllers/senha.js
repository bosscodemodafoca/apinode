import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import auth from '../middlewares/auth'
import Admin from '../models/admin'
import Motoboy from '../models/motoboy'
import Loja from '../models/loja'
import Codigo from '../models/codigo'
import email from '../../config/email'
import moment from 'moment';

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const router = express.Router()

router.post('/esqueci/:tipo', async (req, res) => {
    console.log("esqueci")
  const tipo = req.params.tipo
  let User = null
  switch (tipo) {
    case 'admin':
      User = Admin
      break
    case 'loja':
      User = Loja
      break
    case 'motoboy':
      User = Motoboy
      break
    default:
      return res.status(404).send()
  }

  const user = await User.findOne({
    $or: [{ celular: req.body.user }, { email: req.body.user }]
  })
  if (!user) return res.status(400).send({ error: 'Usuário não encontrado!' })
//   console.log(user.email)

const codigo_valido = await Codigo.findOne({
    usuario:user._id,
    validade: {
      $gte: moment().toDate()
    }
  })

if (!!codigo_valido){
    return res.status(500).send({ error: 'Você já solicitou um código de recuperação, verifique seu e-mail!' })
}

  const codigo = await Codigo.create({
      tipo,
      usuario:user._id,
      codigo:getRandomInt(100000,999999),
      tentativas:0,
      validade:moment().add(1,'days').toDate()
  })
  
 
  email.sendMail({
    from: 'Aplicativo DisqMotoboy <aplicativo.disqmotoboy@gmail.com>', // Quem está mandando
    to: user.nome+' <'+user.email+'>', // Para quem o e-mail deve chegar
    subject: 'Recuperação de Senha', // O assunto
    html: "<strong>Disq Motoboy!</strong>"
    +"<p>Use esse código para recuperar sua senha!</p>"
    +"<H1>"+codigo.codigo+"</H1>"
    +"<p>Ele é válido por 24 horas</p>", 
    }, async function(err){
        if(err){
          await Codigo.find({
            codigo:codigo.codigo
          }).remove().exec()
            return res.status(500).send({ error: 'Não foi possível enviar o e-mail de recuperação!' })
          }

            return res.send({ msg: 'E-mail de recuperação enviado para '+user.email })
    });
})

router.post('/verificar/:tipo', async (req, res) => {
console.log("verificar")
  const tipo = req.params.tipo
  const codigo = req.body.codigo
  console.log(codigo)
  let User = null
  switch (tipo) {
    case 'admin':
      User = Admin
      break
    case 'loja':
      User = Loja
      break
    case 'motoboy':
      User = Motoboy
      break
    default:
      return res.status(404).send()
  }

//   console.log(user.email)
const codigo_valido = await Codigo.findOne({
    codigo:codigo,
    tipo:tipo,
    validade: {
      $gte: moment().toDate()
    }
  })

if (!codigo_valido){
    return res.status(401).send({ error: 'Código Inválido!' })
}


return res.send()

})

router.post('/novasenha/:tipo', async (req, res) => {

      const tipo = req.params.tipo
      const codigo = req.body.codigo
      const password = await bcrypt.hash(req.body.password, 10)
      
     
      let User = null
      switch (tipo) {
        case 'admin':
          User = Admin
          break
        case 'loja':
          User = Loja
          break
        case 'motoboy':
          User = Motoboy
          break
        default:
          return res.status(404).send()
      }
    
    //   console.log(user.email)
    const codigo_valido = await Codigo.findOne({
        codigo:codigo,
        tipo:tipo,
        validade: {
          $gte: moment().toDate()
        }
      })
    
    if (!codigo_valido){
        return res.status(401).send({ error: 'Código Inválido!' })
    }
    

    const user = await User.findByIdAndUpdate(codigo_valido.usuario, {password}, {
        new: true
      })
    
      await Codigo.find({
        codigo:codigo
      }).remove().exec()


    
    return res.send({ msg: 'Senha alterada com sucesso!'})
    
    })

module.exports = app => app.use('/senha', router)
