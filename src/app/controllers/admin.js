import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import auth from '../middlewares/auth'
import Motoboy from '../models/motoboy'
import Loja from '../models/loja'
import Os from '../models/os'
import Pagamento from '../models/pagamento'
import Recebimento from '../models/recebimento'
import moment from 'moment'
const router = express.Router()

// router.get('/', async (req, res) => {
//   const result = await Os.find()
//   return res.send(result)
// })

// apenas usuarios autenticados a partir daqui
router.use(auth)
// ###############################################################################
router.post('/areceber', async (req, res) => {
  // apena usuario admin
  if (req.userTipo != 'admin') return res.status(401).send()
  const loja_id = req.body.loja
  const loja = await Loja.findById(loja_id)
  // busca as os que nao foram pagas de determinada loja
  // ela nao pode ser pendente ou cancelada
  // ordenada

  const os = await Os.find({
    loja: loja_id,
    pago_loja: { $ne: 'PAGO' }
  })
    .and([{ status: { $ne: 'PENDENTE' } }, { status: { $ne: 'CANCELADA' } }])
    .populate('loja')
    .populate('motoboy')
    .sort({ loja: 1 })
    .sort({ data_contabilidade: -1 })
    .sort({ turno: 1 })
    .sort({ motoboy: 1 })

  var turnos = []
  var valor_total = 0

  var temp_valor = 0
  var temp_os = []
  var data_atual = ''
  var turno_atual = ''
  var primeiro = true
  var motoboy_atual = ''

  // percorre todas as os
  // identificando a mudança de turno motoboy ou data
  // para ser calculado corretamente a diaria

  os.map((item, index) => {
    // se ela possui um motoboy
    if (item.motoboy) {
      // verifica se ainda esta no mesmo turno/motoboy/dia
      //se nao tiver que dizer que é um turno diferente
      // entao um novo registro
      if (
        data_atual.toString() != item.data_contabilidade.toString() ||
        turno_atual != item.turno ||
        motoboy_atual.toString() != item.motoboy._id.toString()
      ) {
        console.log('sim')
        if (primeiro) {
          primeiro = false
        } else {
          const novo = {
            valor: temp_valor,
            os: temp_os,
            data: data_atual,
            turno: turno_atual,
            motoboy: motoboy_atual,
            loja: loja_id
          }
          turnos.push(novo)
          valor_total += parseFloat(temp_valor)
        }

        data_atual = item.data_contabilidade
        turno_atual = item.turno
        temp_valor = parseFloat(
          item.loja.turnos[turno_atual - 1].diaria_loja.value
        )
        temp_os = []
      }

      temp_os.push(JSON.parse(JSON.stringify(item)))
      temp_valor += parseFloat(item.valor.value)
      motoboy_atual = item.motoboy._id
    }
  })

  const novo = {
    valor: temp_valor,
    os: temp_os,
    data: data_atual,
    motoboy: motoboy_atual,
    turno: turno_atual,
    loja: loja_id
  }
  if (!primeiro) turnos.push(novo)
  // somando o valor total
  valor_total += parseFloat(temp_valor)
  // retorna o valor total e a lista de turnos com seus valores a pagar e lista de os
  const resultado = { valor_total, turnos: turnos }
  // console.log(resultado)
  return res.status(200).send(resultado)
})
// ###############################################################################
// ###############################################################################
router.post('/apagar', async (req, res) => {
  if (req.userTipo != 'admin') return res.status(401).send()
  const motoboy_id = req.body.motoboy

  const motoboy = await Motoboy.findById(motoboy_id)
  // pesquisa todas as os nao pagas
  //  faz o processo semelhante ao areceber
  const os = await Os.find({
    motoboy: motoboy_id,
    pago_motoboy: { $ne: 'PAGO' }
  })
    .and([{ status: { $ne: 'PENDENTE' } }, { status: { $ne: 'CANCELADA' } }])
    .populate('loja')
    .populate('motoboy')
    .sort({ loja: 1 })
    .sort({ data_contabilidade: -1 })
    .sort({ turno: 1 })
    .sort({ motoboy: 1 })

  var turnos = []
  var valor_total = 0

  var temp_valor = 0
  var temp_os = []
  var data_atual = ''
  var turno_atual = ''
  var primeiro = true
  var loja_atual = ''
  os.map((item, index) => {
    if (
      data_atual.toString() != item.data_contabilidade.toString() ||
      turno_atual != item.turno ||
      loja_atual != item.loja._id
    ) {
      // console.log(loja_atual+'|'+item.loja._id)
      if (primeiro) {
        primeiro = false
      } else {
        const novo = {
          valor: temp_valor,
          os: temp_os,
          data: data_atual,
          turno: turno_atual,
          motoboy: motoboy_id,
          loja: item.loja._id
        }
        turnos.push(novo)
        valor_total += parseFloat(temp_valor)
      }

      data_atual = item.data_contabilidade
      turno_atual = item.turno
      // console.log("loja.diaria",item.loja.turnos[turno_atual-1].diaria_motoboy.value)
      temp_valor = parseFloat(
        item.loja.turnos[turno_atual - 1].diaria_motoboy.value
      )
      temp_os = []
    }
    temp_os.push(JSON.parse(JSON.stringify(item)))
    console.log('item.valor', item.valor.value)
    temp_valor += parseFloat(item.valor.value)
    loja_atual = item.loja._id
  })

  const novo = {
    valor: temp_valor,
    os: temp_os,
    data: data_atual,
    motoboy: motoboy_id,
    turno: turno_atual,
    loja: loja_atual
  }
  if (!primeiro) turnos.push(novo)
  valor_total += parseFloat(temp_valor)
  const resultado = { valor_total, turnos: turnos }
  console.log(resultado)
  return res.status(200).send(resultado)
})
// ###############################################################################
router.post('/pagos', async (req, res) => {
  if (req.userTipo != 'admin') return res.status(401).send()
  // o relatorio de pagos sao referentes a um periodo
  // que e enviado pelo aplicativo
  // é uma pesquisa simples por perido
  // com o retorno da listagens de os tambem 
  // e ordenado
  var de = moment(req.body.de).startOf('day')
  var ate = moment(req.body.ate).endOf('day')
  const pagamentos = await Pagamento.find({
    created_at: { $gte: de, $lte: ate }
  })
    .sort({ created_at: -1 })
    .populate('loja')
    .populate('motoboy')
    .populate({ path: 'os', populate: { path: 'motoboy' } })
    .populate({ path: 'os', populate: { path: 'loja' } })
  return res.status(200).send(pagamentos)
})
// ###############################################################################
router.post('/recebidos', async (req, res) => {
  // identico ao pagos
  if (req.userTipo != 'admin') return res.status(401).send()
  var de = moment(req.body.de).startOf('day')
  var ate = moment(req.body.ate).endOf('day')
  const recebimentos = await Recebimento.find({
    created_at: { $gte: de, $lte: ate }
  })
    .sort({ created_at: -1 })
    .populate('loja')
    .populate('motoboy')
    .populate('os')

  return res.status(200).send(recebimentos)
})
// ###############################################################################
router.post('/pagar', async (req, res) => {
  if (req.userTipo != 'admin') return res.status(401).send()
  const resultado = req.body

  resultado.turnos.map(async (item, index) => {
    let oss = []
    item.os.map(async (os, i) => {
      // console.log(os._id)
      oss.push(os._id)
      await Os.findByIdAndUpdate(os._id, { pago_motoboy: 'PAGO' })
      //  console.log(os._id,"PAGO")
    })
    // console.log('loja', item.loja)
    // a funcao pagar cria um registro separado 
    // com todas as informacoes de referencia 
    // e tambem a lista de os
    // a lista de os e necessaria para ver o detalhamento
    // e tambem caso seja necessário o cancelamento
    const pag = await Pagamento.create({
      valor: item.valor,
      loja: item.loja,
      motoboy: item.motoboy,
      turno: item.turno,
      data_contabilidade: item.data,
      os: oss
    })
    // console.log(pag)
  })

  return res.status(201).send()
})

// ###############################################################################
router.post('/receber', async (req, res) => {
  // identico o pagar  
  if (req.userTipo != 'admin') return res.status(401).send()
  const resultado = req.body
  resultado.turnos.map(async (item, index) => {
    let oss = []
    item.os.map(async (os, i) => {
      // console.log(os._id)
      oss.push(os._id)
      await Os.findByIdAndUpdate(os._id, { pago_loja: 'PAGO' })
      //  console.log(os._id,"PAGO")
    })
    console.log('loja', item.loja)
    const pag = await Recebimento.create({
      valor: item.valor,
      loja: item.loja,
      motoboy: item.motoboy,
      turno: item.turno,
      data_contabilidade: item.data,
      os: oss
    })
    console.log(pag)
  })
  return res.status(201).send()
})

router.post('/cancelarpagamento', async (req, res) => {
  if (req.userTipo != 'admin') return res.status(401).send()
  const _id = req.body._id
// cancelar pagamento percorre todas as os do pagamento
// e muda seu status para a paga
// e depois deleta o registro de pago
  const pagamento = await Pagamento.findById(_id)

  pagamento.os.map(async (item, index) => {
    await Os.findByIdAndUpdate(item._id, { pago_motoboy: 'A PAGAR' })
  })
  console.log('apagando', _id)
  Pagamento.findByIdAndRemove(_id, (erro, doc) => {
    if (erro) console.log('erro', erro)
  })

  return res.status(200).send()
})

router.post('/cancelarrecebimento', async (req, res) => {
  if (req.userTipo != 'admin') return res.status(401).send()
  const _id = req.body._id
// identico ao cancelaar pagamento
  const pagamento = await Recebimento.findById(_id)

  pagamento.os.map(async (item, index) => {
    await Os.findByIdAndUpdate(item._id, { pago_loja: 'A PAGAR' })
  })
  console.log('apagando', _id)
  Recebimento.findByIdAndRemove(_id, (erro, doc) => {
    if (erro) console.log('erro', erro)
  })

  return res.status(200).send()
})

module.exports = app => app.use('/admin', router)
