
export default {
    // codigo pra gerar a token criptografada
    secretToken: "507e5b7316af91f19557c667490b0fab",
    secretRefreshToken: "9727135627527233653bba301906babb",
    // tempo de vida do token

    tokenLife: 60 * 60 * 12,  //12 horas
    refreshTokenLife: 60 * 60 * 24 * 60 //  60 dias
}