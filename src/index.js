
import bodyParser from 'body-parser'
import cors from 'cors'
import authConfig from './config/jwtConfig'
import jwt from "jsonwebtoken"
import moment from 'moment'

var app = require('express')();
var server = require('http').Server(app);
const io = require('socket.io')(server,{ pingTimeout: 10000 })

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}))

io.set('origins', '*:*');
io.on('connection', socket =>{
    console.log(`Socket conectado: ${socket.id}`)
    socket.on('LOGAR', data => {
        console.log("logado")
        jwt.verify(data, authConfig.secretToken, (err, decoded) => {
            if (!err){
                socket.join(decoded.id)
                io.to(decoded.id).emit('LOGGED')
            }else{
                console.log("Erro no login")
            }
          })
    })
})

app.use((req, res, next) => {
    req.io = io;
    return next();
})

require('./app/controllers/index')(app)

const port = 3050

server.listen(port,()=>{
    console.log(moment())
    console.log(moment())
    console.log("SERVER ON "+port)

    

})
